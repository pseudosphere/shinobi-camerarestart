const connectionTester = require('connection-tester')

module.exports = function(s,config,lang,app,io){
    // pull in functions from main code
    const {
        getActiveMonitor,
        monitorRestart,
    } = require('../../monitor/utils.js')(s,config,lang)

    async function pingActiveMonitors() {
        // crawl through groups and monitors
        if (s.group) {
            for (const groupKey in s.group) {
                const group = s.group[groupKey]
                for (const monitorId in group.activeMonitors) {
                    activeMonitor = getActiveMonitor(groupKey, monitorId);
                    monitorConfig = s.group[groupKey].rawMonitorConfigurations[monitorId];
                    // check if monitor is in watch-only mode (status code = 2)
                    if (activeMonitor.monitorStatusCode==2) {
                        // try connection to the monitor
                        const pingResult = await testConnection(monitorConfig.host, monitorConfig.port, 2000);
                        if (pingResult.response.success == true ) {
                            // monitor is online, so remove time of first failed ping if it is defined
                            if (activeMonitor.firstFailedPing) {
                                delete(activeMonitor.firstFailedPing);
                            }
                        } else {
                            if (!activeMonitor.firstFailedPing) {
                                // first time connection has failed, so record time
                                activeMonitor.firstFailedPing = Date.now();
                                console.log(`Ping failed for ${monitorId}. Recorded at ${activeMonitor.firstFailedPing}.`);
                            } else {
                                // check how long it has been since the camera first failed to respond to connection requests, and restart if > 10 minutes
                                console.log(`Ping to ${monitorId} failed.`);
                                if(Date.now()-activeMonitor.firstFailedPing > 1000*60*10) {
                                    console.log(`Restarting monitor ${monitorId}`);
                                   monitorRestart(monitorConfig);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    // reproduction of asyncConnectionTest from main code
    function testConnection(host,port,timeout) {
        return new Promise( (resolve) => {
            connectionTester.test(host,port,timeout,(err,response) => {
                resolve( {
                    err,
                    response,
                } )
            } )
        } )
    }

    // check cameras every 5 minutes
    setInterval(pingActiveMonitors, 1000*60*5);
}
