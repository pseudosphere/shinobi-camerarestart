# shinobi cameraRestart



## Description

Custom Auto Load Module for [Shinobi CCTV](https://shinobi.video/) that automatically restarts a camera if it is left on "Watch-Only" mode but disconnects. Shinobi will sometimes gracefully restart the camera so that it is in "Died"/"Starting" mode until the camera comes back online. Frequently, especially if the camera is in the middle of recording a motion event, it will stay in "Watching" mode while showing the last video segment whenever the stream is launched. Even if the camera is powered back on and/or reconnects to the network, the camera remains stuck in this state without launching the new stream. [Issue #491](https://gitlab.com/Shinobi-Systems/Shinobi/-/issues/491)

This module resolves this issue by pinging each active camera every 5 minutes. If a camera is in "Watch-Only" mode but is not detected by pings for more than 10 minutes, the module will send a restart command to the camera, putting it back in "Died"/"Starting" mode so that it will successfully come back online when the camera is powered back on and reconnects.

## Install

Download the [cameraRestart/index.js](cameraRestart/index.js) file and put into a subdirectory in the customAutoLoad directory in your Shinobi install and restart Shinobi.


## License

Provided under  [GNU General Public License v3.0](LICENSE)